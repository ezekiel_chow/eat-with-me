//
//  LeftMenuViewController.h
//  EatWithMe
//
//  Created by Ezekiel Chow on 4/28/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKLoginKit.h>

@interface LeftMenuViewController : UIViewController <FBSDKLoginButtonDelegate>

@end