//
//  MainViewController.h
//  EatWithMe
//
//  Created by Ezekiel Chow on 5/13/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SIAlertView.h>
#import <MBProgressHUD.h>

@interface ProfilePictureViewController : UIViewController<UIImagePickerControllerDelegate>
{
    MBProgressHUD *HUD;
}

@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@end
