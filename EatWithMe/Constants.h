//
//  Constants.h
//  EatWithMe
//
//  Created by Ezekiel Chow on 6/1/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#ifndef EatWithMe_Constants_h
#define EatWithMe_Constants_h
#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString *const kFoursquareClientID;
FOUNDATION_EXPORT NSString *const kFoursquareClientSecret;

#endif
