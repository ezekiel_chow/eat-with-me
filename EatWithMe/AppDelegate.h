//
//  AppDelegate.h
//  EatWithMe
//
//  Created by Ezekiel Chow on 4/26/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

