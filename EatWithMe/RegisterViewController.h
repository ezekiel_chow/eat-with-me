//
//  ViewController.h
//  EatWithMe
//
//  Created by Ezekiel Chow on 4/26/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <MBProgressHUD.h>
#import <FBSDKLoginKit.h>
#import <FBSDKCoreKit.h>

@class MBProgressHUD;

@interface RegisterViewController : UIViewController <FBSDKLoginButtonDelegate>
{
    MBProgressHUD *HUD;
}

@property (nonatomic, retain) NSDictionary *userDetails;

@end

