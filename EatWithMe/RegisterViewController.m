//
//  ViewController.m
//  EatWithMe
//
//  Created by Ezekiel Chow on 4/26/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import "RegisterViewController.h"
#import <FBSDKCoreKit.h>
#import <FBSDKLoginKit.h>
#import <MBProgressHUD.h>
#import <SIAlertView.h>

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    [loginButton setFrame:CGRectMake(self.view.frame.size.width/2 - 50, self.view.frame.size.height/2 -50, loginButton.frame.size.width, loginButton.frame.size.height)];
    
    //permissions for login button
    loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    
    [self.view addSubview:loginButton];
    
    //check if user is login
    if ([FBSDKAccessToken currentAccessToken])
    {
        // User is logged in, do work such as go to next view controller.
        [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
        [self performSegueWithIdentifier:@"reg_profileSegue" sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setting User Data

- (void)_loadData {
    
    float progress = 0.0f;
    while (progress < 1.0f)
    {
        progress += 0.01f;
        HUD.progress = progress;
        usleep(50000);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            // handle response
            
            if (!error) {
                NSDictionary *userData = (NSDictionary *)result;
                self.userDetails = userData;
                
                NSString *facebookID = userData[@"id"];
                NSString *name = userData[@"name"];
                NSString *gender = userData[@"gender"];
                NSString *firstname = userData[@"first_name"];
                NSString *lastname = userData[@"last_name"];
                NSString *email = userData[@"email"];
                
                //get profile picture
                NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
                
                NSURLRequest *urlRequest = [NSURLRequest requestWithURL:pictureURL];
                
                // Run network request asynchronously
                [NSURLConnection sendAsynchronousRequest:urlRequest
                                                   queue:[NSOperationQueue mainQueue]
                                       completionHandler:
                 ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                     if (connectionError == nil && data != nil) {
                         // Set the image in the imageView
                         // ...
                         UIImage *profilePicture = [UIImage imageWithData:data];
                         
                        // [self.lastNameTextField setText:lastname];
                        // [self.firstNameTextField setText:firstname];
                        // [self.userFbProfilePicture setImage:profilePicture];
                        // [self.emailTextField setText:email];
                     }
                 }];
            }
            else
            {
                SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"No Internet Connection" andMessage:@"Please Check Your Internet before you proceed"];
                
                [alertView addButtonWithTitle:@"Ok"
                                         type:SIAlertViewButtonTypeDefault
                                      handler:^(SIAlertView *alert) {
                                          
                                      }];
                alertView.willShowHandler = ^(SIAlertView *alertView) {
                    //NSLog(@"%@, willShowHandler", alertView);
                };
                alertView.didShowHandler = ^(SIAlertView *alertView) {
                    //NSLog(@"%@, didShowHandler", alertView);
                };
                alertView.willDismissHandler = ^(SIAlertView *alertView) {
                    //NSLog(@"%@, willDismissHandler", alertView);
                };
                alertView.didDismissHandler = ^(SIAlertView *alertView) {
                    HUD = [[MBProgressHUD alloc]initWithView:self.view];
                    [self.view addSubview:HUD];
                    
                    [HUD showWhileExecuting:@selector(_loadData) onTarget:self withObject:nil animated:YES];
                };
                
                alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                
                [alertView show];
            }
            
        }];
    });
    
    sleep(2);
}

#pragma mark - Self Functions

-(void)testingFunc
{
    NSLog(@"It is working!!");
}

-(void)saveDataLocally
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    [defaults setObject:_userDetails forKey:@"profileData"];
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"reg_profileSegue"])
    {
        [self saveDataLocally];
    }
}

#pragma mark - Facebook Login Button Delegate
- (void)  loginButton:(FBSDKLoginButton *)loginButton
didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result
                error:(NSError *)error
{
    if (error) {
        // Process error
        NSLog(@"error here?");

    }
    else if (result.isCancelled) {
        // Handle cancellations
        NSLog(@"why u cancel me?");
    }
    else {
        // Navigate to other view
        NSLog(@"move to other view");
        [self performSegueWithIdentifier:@"reg_profileSegue" sender:self];

    }
}

@end
