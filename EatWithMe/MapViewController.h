//
//  MapViewController.h
//  EatWithMe
//
//  Created by Ezekiel Chow on 5/31/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController <CLLocationManagerDelegate>

@end
