//
//  RESideMenu+SidemenuViewController.m
//  EatWithMe
//
//  Created by Ezekiel Chow on 4/27/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import "RESideMenu+SidemenuViewController.h"

@implementation RESideMenu (SidemenuViewController)

- (void)awakeFromNib
{
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    self.rightMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rightMenuViewController"];
}

@end
