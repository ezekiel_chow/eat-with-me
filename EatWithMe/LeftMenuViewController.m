//
//  LeftMenuViewController.m
//  EatWithMe
//
//  Created by Ezekiel Chow on 4/28/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import "LeftMenuViewController.h"
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <FBSDKLoginKit.h>
#import <FBSDKCoreKit.h>
#import "RESideMenu+SidemenuViewController.h"

@implementation LeftMenuViewController

- (IBAction)logoutBtnPressed:(id)sender
{
    //logout user
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc]init];
    [loginManager logOut];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Base" bundle:nil];
    UIViewController *contentView = [storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];

    [self.sideMenuViewController setContentViewController:contentView animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton
{
    
}
@end