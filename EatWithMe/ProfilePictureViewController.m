//
//  MainViewController.m
//  EatWithMe
//
//  Created by Ezekiel Chow on 5/13/15.
//  Copyright (c) 2015 Ezekiel Chow. All rights reserved.
//

#import "ProfilePictureViewController.h"
#import "RESideMenu+SidemenuViewController.h"
#import <FBSDKCoreKit.h>
#import <FBSDKLoginKit.h>
#import <SIAlertView.h>
#import <UIImage+ResizeMagick.h>

@interface ProfilePictureViewController ()

@end

@implementation ProfilePictureViewController

-(void)viewWillAppear:(BOOL)animated
{

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.profilePictureImageView.layer.cornerRadius = self.profilePictureImageView.frame.size.width/2;
    self.profilePictureImageView.clipsToBounds = YES;
    self.profilePictureImageView.layer.borderWidth = 2.0f;
    self.profilePictureImageView.layer.borderColor = [UIColor blackColor].CGColor;
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 NSDictionary *userData = (NSDictionary *)result;
                 
                 NSString *facebookID = userData[@"id"];
                 NSString *name = userData[@"name"];
                 NSString *gender = userData[@"gender"];
                 NSString *firstname = userData[@"first_name"];
                 NSString *lastname = userData[@"last_name"];
                 NSString *email = userData[@"email"];
                 
                 //get profile picture
                 NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
                 
                 NSURLRequest *urlRequest = [NSURLRequest requestWithURL:pictureURL];
                 
                 // Run network request asynchronously
                 [NSURLConnection sendAsynchronousRequest:urlRequest
                                                    queue:[NSOperationQueue mainQueue]
                                        completionHandler:
                  ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                      if (connectionError == nil && data != nil) {
                          // Set the image in the imageView
                          // ...
                          UIImage *profilePicture = [UIImage imageWithData:data];
                          
                          
                          [self.profilePictureImageView setImage:profilePicture];
                          // [self.lastNameTextField setText:lastname];
                          // [self.firstNameTextField setText:firstname];
                          // [self.userFbProfilePicture setImage:profilePicture];
                          // [self.emailTextField setText:email];
                      }
                  }];
             }
             else
             {
                 SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"No Internet Connection" andMessage:@"Please Check Your Internet before you proceed"];
                 
                 [alertView addButtonWithTitle:@"Ok"
                                          type:SIAlertViewButtonTypeDefault
                                       handler:^(SIAlertView *alert) {
                                           
                                       }];
                 alertView.willShowHandler = ^(SIAlertView *alertView) {
                     //NSLog(@"%@, willShowHandler", alertView);
                 };
                 alertView.didShowHandler = ^(SIAlertView *alertView) {
                     //NSLog(@"%@, didShowHandler", alertView);
                 };
                 alertView.willDismissHandler = ^(SIAlertView *alertView) {
                     //NSLog(@"%@, willDismissHandler", alertView);
                 };
                 alertView.didDismissHandler = ^(SIAlertView *alertView) {
                     HUD = [[MBProgressHUD alloc]initWithView:self.view];
                     [self.view addSubview:HUD];
                     
                     [HUD show:YES];
                 };
                 
                 alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
                 
                 [alertView show];
             }
         }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sideMenuPressed:(id)sender
{
    [self.sideMenuViewController presentLeftMenuViewController];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//camera button pressed
- (IBAction)takeAPicture:(id)sender
{
    [[[UIActionSheet alloc] initWithTitle:nil
                                 delegate:self
                        cancelButtonTitle:@"Close"
                   destructiveButtonTitle:nil
                        otherButtonTitles:@"Take photo", @"Choose photo", nil]
     showInView:self.view];
}

#pragma mark - action sheet delegates
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self takePhoto]; break;
        /*case 1:
            [self choosePhoto];break; */
    }
}

-(void)takePhoto
{
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    
    #if TARGET_IPHONE_SIMULATOR
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    #else
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    #endif
    
    imagePickerController.editing = YES;
    imagePickerController.delegate = (id)self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}
/*
- (void)imagePickerControllerDidCancel:(UIImagePickerController *) picker {
    
    [[picker parentViewController] dismissViewControllerAnimated:YES completion:^{}];
}
*/

#pragma mark - Image picker delegate methdos
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    /*
    // Resize the image from the camera
    UIImage *scaledImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(photo.frame.size.width, photo.frame.size.height) interpolationQuality:kCGInterpolationHigh];
    // Crop the image to a square (yikes, fancy!)
    UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -photo.frame.size.width)/2, (scaledImage.size.height -photo.frame.size.height)/2, photo.frame.size.width, photo.frame.size.height)];
     */
    
    UIImage *resizedImage = [image resizedImageByMagick:@"180x180#"];
    // Show the photo on the screen
    [self.profilePictureImageView setImage:resizedImage];
    [picker dismissModalViewControllerAnimated:NO];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissModalViewControllerAnimated:NO];
}


@end
